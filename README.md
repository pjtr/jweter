# JWeter

![JWeter icon](https://bytebucket.org/pjtr/jweter/raw/master/src/net/luminis/jweter/jweter1.png)

## Overview

JWeter is a tool for analyzing and visualing JMeter result (.jtl) files. It creates time series and distribution graphs, differentiating the various samples from your JMeter script (which for example could represent the various steps in a web flow) with different colors / graph labels, helping you to quickly spot the requests that have long response times. 

## Usage

Download the jar file and run it with `java -jar JWeter.jar`, or double click the jar file (depends on OS). A wizzard will appear that will guide you through the steps to create a nice graph or error analysis.

The graphs can be saved as png by right clicking and selecting "save as". Use mouse gestures to zoom in (select rectangle) or zoom out (move to top left). 

## JMeter .jtl files

To let JMeter create .jtl files, add a [Simple Data Writer](http://jmeter.apache.org/usermanual/component_reference.html#Sample_Result_Save_Configuration) listener to the test plan, enter a file name (the .jtl extension is commonly used, but not mandatory) and make sure to select "Save As XML" in the configuration panel. 


## License

JWeter is open source and licensed under LGPL (see the NOTICE.txt and LICENSE.txt files in the distribution). As of the LGPL license, all modifications and additions to the source code must be published as (L)GPL as well.

## Acknowledgements

The beautiful JWeter icon is created by Ivo Domburg.

## Known issues

On windows, a warning message appears in de console about not being able to open/create prefs root node; this is coming from the logger in the (Java) preference implementation, see (http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6809488).

## Feedback

Of course, all feedback is welcome. Mail the author (peter dot doornbosch) at luminis dot eu, or create an issue at <https://bitbucket.org/pjtr/jweter/issues>.
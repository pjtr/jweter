/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import org.jfree.data.xy.XYSeriesCollection
import org.jfree.data.xy.XYSeries
import org.jfree.chart.JFreeChart
import org.jfree.chart.ChartFactory
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.axis.LogarithmicAxis
import org.jfree.chart.ChartFrame
import javax.xml.parsers.SAXParserFactory
import org.xml.sax.InputSource
import org.jfree.data.statistics.HistogramDataset
import org.jfree.data.statistics.HistogramType
import org.jfree.chart.renderer.xy.ClusteredXYBarRenderer
import javax.swing.ProgressMonitorInputStream
import org.jfree.chart.axis.DateAxis
import org.jfree.chart.renderer.xy.StandardXYItemRenderer
import java.text.NumberFormat
import org.jfree.chart.labels.StandardXYToolTipGenerator
import java.text.SimpleDateFormat
import groovy.swing.SwingBuilder
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Point
import org.jfree.data.general.DefaultPieDataset
import org.jfree.chart.ChartPanel
import javax.swing.BoxLayout
import java.awt.Dimension
import org.jfree.data.category.DefaultCategoryDataset
import org.jfree.data.UnknownKeyException
import org.jfree.chart.plot.XYPlot
import org.jfree.chart.renderer.category.BarRenderer
import javax.swing.JColorChooser
import org.jfree.chart.renderer.category.StandardBarPainter
import org.jfree.chart.axis.CategoryLabelPositions


class JMeterResults {

    static {
        DefaultCategoryDataset.metaClass.addOrIncrement = { java.lang.Comparable rowKey, java.lang.Comparable columnKey ->
            try {
                delegate.incrementValue(1, rowKey, columnKey)
            } catch (UnknownKeyException notYetThere) {
                delegate.addValue(1, rowKey, columnKey)
            }
        }
    }

    File file
    boolean validJtlVersion
    List allResults = []
    Long startTest = Long.MAX_VALUE
    Long endTest = 0

    Set selectedLabels = [] as Set

    File getFile() {
        file
    }
    
    public void setFile(File file) {
        this.file = file
    }

    public int getNrOfSamples() {
        return allResults.size()
    }

    public Date getStartTest() {
        return new Date(startTest)
    }

    public Date getEndTest() {
        return new Date(endTest)
    }

    public long getDuration() {
        (endTest - startTest) / 1000
    }

    public Integer getFastest() {
        allResults.collect { it.time }.min()        
    }

    public Integer getSlowest() {
        allResults.collect { it.time }.max()
    }

    public Set getLabels() {
        Set tags = [] as Set
        allResults.each { tags.add(it.label) }
        tags
    }

    /**
     * Return results for the selected labels only
     * @return
     */
    List getSelectedResults() {
        allResults.findAll { selectedLabels.contains(it.label) }
    }

    /**
     * Returns a list with: count (nr. samples), min and max for the indicated label
     */
    public List getCountMinMax(label) {
        def filtered = allResults.findAll{ result -> result.label == label }
        [ filtered.size(), filtered.time.min(), filtered.time.max()]
    }
    
    public int getMinForSelectedLabels() {

    }

    public Integer getMaxForSelectedLabels() {
        allResults.findAll { selectedLabels.contains(it.label) }.time.max()
    }

    public void selectLabel(String label, boolean on) {
        if (on) {
            selectedLabels.add(label)
        }
        else {
            selectedLabels.remove(label)
        }
    }

    def parseJMeterResult(def frame) {
        parseWithSax(file, frame)
    }

    def parseWithXmlSlurper(File file, def listener) {

        def xml = new XmlSlurper().parse(file)

        xml.httpSample.each { sample ->

            SampleResult result = new SampleResult()
            result.label = sample.@lb.toString()
            result.timestamp = sample.@ts.toString() as long
            result.time = sample.@t.toString() as int
            result.success = sample.@s.toString() as boolean
            result.responseCode = sample.@rs.toString()
            allResults << result

            // Detect min and max time right away
            if (result.timestamp < this.@startTest) {   // Expliciet field access necessary because closure is (apparently) not in lexical scope of field
                startTest = result.timestamp
            }
            if (result.timestamp > this.@endTest) {
                endTest = result.timestamp
            }
        }
    }

    def parseWithSax(File file, def frame) {

        def handler = new SaxSampleHandler()
        def reader = SAXParserFactory.newInstance().newSAXParser().XMLReader
        reader.contentHandler = handler
        reader.errorHandler = handler

        def fileStream = new ProgressMonitorInputStream(frame, "Reading ${file}", new FileInputStream(file));
        def monitor = fileStream.progressMonitor
        def inputStream = new BufferedInputStream(fileStream)

        try {
            reader.parse(new InputSource(inputStream))
        }
        finally {
            monitor.close()
        }

        allResults = handler.list
        validJtlVersion = handler.jtl12
        def timestamps = allResults.collect { it.timestamp }
        startTest = timestamps.min()
        endTest = timestamps.max()
    }
    
    def createGraph(boolean logarithmicAxis, boolean relativeDomainAxis) {

        XYSeriesCollection c = new XYSeriesCollection()
        
        selectedLabels.each { currentTag ->
            def filtered = allResults.findAll { it.label == currentTag }

            // Reducing the number of samples is necessary for very large files (graph can't handle it).
            // The filter below is too simple, because it might accidently remove the extremes; it would be better to remove samples that are very much alike.
            if (allResults.size() > 10000000) {
                println "filtering because of too much samples"
                def filtered2 = []
                filtered.eachWithIndex { it, index ->
                    if (index % 5 == 0)
                    filtered2 << it
                }
                println "from ${filtered.size()} to ${filtered2.size()}"
                filtered = filtered2
            }

            XYSeries series = new XYSeries(currentTag)
            filtered.each { sample ->
                if (relativeDomainAxis) {
                    series.add( (sample.timestamp - this.@startTest) / 1000, sample.time)
                }
                else {
                    series.add(sample.timestamp, sample.time)
                }
            }
            c.addSeries(series)
        }

        JFreeChart chart = ChartFactory.createScatterPlot(file.name,
                relativeDomainAxis? "time (seconds since start of test)": "time", "response time (ms)", c, PlotOrientation.VERTICAL, true, true, false)

        if (logarithmicAxis) {
            def axis = new LogarithmicAxis("response time (ms)")
            chart.getPlot().setRangeAxis(axis)
        }
        if (! relativeDomainAxis) {
            def axis = new DateAxis()
            chart.plot.domainAxis = new DateAxis()

            // Create a customized renderer in order to render the tooltips differently
            StandardXYToolTipGenerator tooltipGenerator = new StandardXYToolTipGenerator("{0}: {1}, {2}",
                    new SimpleDateFormat("HH:mm:ss"), NumberFormat.getInstance());
            StandardXYItemRenderer renderer = new StandardXYItemRenderer(StandardXYItemRenderer.SHAPES, tooltipGenerator, null);
            renderer.setShapesFilled(true);
            chart.plot.renderer = renderer;
        }
        ChartFrame chartFrame = new ChartFrame("JWeter - time series", chart)
        chartFrame.locationByPlatform = true
        chartFrame.pack()
        chartFrame.setSize(1050, 800)
        chartFrame.setVisible true
    }

    def createHistogram(int bins, int min, int max, boolean relative, boolean together) {
        HistogramDataset d = new HistogramDataset()
        if (relative) {
            d.type = HistogramType.RELATIVE_FREQUENCY
        }

        if (together) {
            def filtered = allResults.findAll { selectedLabels.contains(it.label) }
            def times = filtered.collect { it.time }
            def values = new double[filtered.size()]
            filtered.eachWithIndex { sample, idx -> values[idx] = sample.time }
            d.addSeries("request", values, bins, min, max)
        }
        else {
            selectedLabels.each { currentTag ->
                def filtered = allResults.findAll { it.label == currentTag }

                def times = filtered.collect { it.time }

                def values = new double[filtered.size()]
                filtered.eachWithIndex { sample, idx -> values[idx] = sample.time }

                d.addSeries(currentTag, values, bins, min, max)
            }
        }

        JFreeChart chart = ChartFactory.createHistogram(file.name, null, null, d, PlotOrientation.VERTICAL, true, true, false)
        def plot = chart.getPlot()
        if (selectedLabels.size() == 1) {
            plot.getRenderer().setMargin(0.2)
        }
        else {
            plot.setRenderer(new ClusteredXYBarRenderer(0.10, false))
        }

        ChartFrame chartFrame = new ChartFrame("JWeter - histogram", chart)
        chartFrame.locationByPlatform = true
        chartFrame.pack()
        chartFrame.setSize(1050, 800)
        chartFrame.setVisible true
    }

    def createErrorAnalysis(boolean showColorPicker) {

        List results = getSelectedResults()

        // PIE 1: successful / unsuccessful samples
        def resultDataset = new DefaultPieDataset()
        resultDataset.setValue('ok', results.count { it.success })
        resultDataset.setValue('error', results.count { ! it.success } )
        def chart1 = ChartFactory.createPieChart('ok/error', resultDataset, false, true, false)
        def green = new Color(36, 145, 48)
        chart1.plot.setSectionPaint('ok', green)
        def red = new Color(161, 39, 39)
        chart1.plot.setSectionPaint('error', red)

        // PIE 2: response codes
        def allResponseCodes = results.inject([] as Set) { acc, sample -> acc << sample.responseCode }
        def rcDataset = new DefaultPieDataset()
        allResponseCodes.each { code ->
            rcDataset.setValue(code, results.count { it.responseCode == code })
        }
        def chart2 = ChartFactory.createPieChart('response codes', rcDataset, false, true, false)
        def colors = [ new Color(227,56,56), new Color(87,87,231), new Color(45,173,45), new Color(251,251,49), new Color(194,66,194), new Color(153,153,0), new Color(134,99,30) ]
        (0..6).each { index ->
            chart2.plot.setSectionPaint(chart2.plot.getSectionKey(index), colors[index])
        }

        // PIE 3: error causes
        def allErrorSamples = results.findAll { !it.success }
        def causes = [:]
        allErrorSamples.each { sample ->
            def cause = sample.errorCause
            causes[cause] = (causes[cause]?: 0) + 1
        }
        def errorCauseDataset = new DefaultPieDataset()
        causes.each { entry ->
            errorCauseDataset.setValue(entry.key, entry.value)
        }
        def chart3 = ChartFactory.createPieChart('error causes', errorCauseDataset, false, true, false)

        // Stacked bar chart: error causes for each label. Two variants: all samples vs. error-samples only
        def tableData = new DefaultCategoryDataset()
        def tableDataErrorsOnly = new DefaultCategoryDataset()
        selectedLabels.each { String currentTag ->
            def samples = results.findAll { it.label == currentTag }
            samples.each { sample ->
                if (sample.success) {
                    tableData.addOrIncrement('Ok', currentTag)
                }
                else {
                    def cause = sample.errorCause
                    tableData.addOrIncrement(cause, currentTag)
                    tableDataErrorsOnly.addOrIncrement(cause, currentTag)
                }
            }
            // Ensure that each tag occurs in this dataset too (in order to make the charts similar)
            if (! tableDataErrorsOnly.getColumnKeys().contains(currentTag)) {
                tableDataErrorsOnly.setValue(0, 'errors', currentTag)
            }
        }

        def chart4 = ChartFactory.createStackedBarChart("Sample error rates", "Label", "Number of samples", tableData, PlotOrientation.VERTICAL, false, true, false)
        def barChartColors = [new Color(45,173,45) ]
        barChartColors.eachWithIndex { color, index ->
            chart4.plot.renderer.setSeriesPaint(index, color)
        }

        def chart5 = ChartFactory.createStackedBarChart("Samples error rates", "Label", "Number of samples", tableDataErrorsOnly, PlotOrientation.VERTICAL, false, true, false)

        [ chart4, chart5 ].each { chart ->
            chart.plot.renderer.setBarPainter(new StandardBarPainter())
            chart.plot.domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
        }

        def swing = new SwingBuilder()
        def frame = swing.frame(title: 'JWeter - error analysis', locationByPlatform: true)

        // Default preferred size is 680x420, so ratio is 1.6190.
        def pieSize = new Dimension(340, 210)

        swing.container(frame, background: Color.white) {
            boxLayout(axis: BoxLayout.Y_AXIS)
            vstrut(10)
            label(text: file.name, font: chart4.title.font)
            vstrut(10)
            panel() {
                boxLayout()
                panel(id: 'chart1', new ChartPanel(chart1)) {
                    current.preferredSize = pieSize
                }
                panel(new ChartPanel(chart2)) {
                    current.preferredSize = pieSize
                }
                panel(new ChartPanel(chart3)) {
                    current.preferredSize = pieSize
                }
            }
            vstrut(30)
            panel(id: 'tabPanel') {
                tabLayout = cardLayout()
                panel(new ChartPanel(chart4), constraints: '1')
                panel(new ChartPanel(chart5), constraints: '2')
            }
            panel() {
                boxLayout(axis: BoxLayout.X_AXIS)
                hstrut(10)
                buttonGroup().with { radioGroup ->
                    radioButton(text: 'show all samples', selected: true, id: 'show_all', buttonGroup: radioGroup, actionPerformed: {
                        tabLayout.first(swing.tabPanel)
                    })
                    radioButton(text: 'error samples only', id: 'errors_only', buttonGroup: radioGroup, actionPerformed: {
                        tabLayout.last(swing.tabPanel)
                    })
                }
                hglue()

                if (showColorPicker) {
                    textField(text: '0', id: 'barIndex', maximumSize: new Dimension(70, 100))
                    button(text: 'color', actionPerformed: {
                        Color newColor = JColorChooser.showDialog(null, 'pick color', chart2.plot.getSectionPaint(chart2.plot.getSectionKey(swing.barIndex.text as int))) //renderer.getSeriesPaint(swing.barIndex.text as int))
                        println "Selected color ${swing.barIndex.text}: new Color(${newColor.red},${newColor.green},${newColor.blue})"
                        chart4.plot.renderer.setSeriesPaint(swing.barIndex.text as int, newColor)
                        [chart2, chart3].each { pie ->
                            pie.plot.setSectionPaint(pie.plot.getSectionKey(swing.barIndex.text as int), newColor)
                        }
                    })
                }
            }
            vstrut(10)
        }

        frame.pack()
        frame.show()
    }
}


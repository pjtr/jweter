/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import java.awt.Toolkit
import java.awt.Image
import java.lang.reflect.Method

/**
 * The startup script for the JWeter tool: command line processing and initialization.
 */

// OSX specific initialization
System.setProperty("com.apple.mrj.application.apple.menu.about.name", "JWeter");
try {
    Class macOsxApplicationClass = Class.forName("com.apple.eawt.Application")
    Method singletonMethod = macOsxApplicationClass.getMethod("getApplication")
    def application = singletonMethod.invoke(null, null)
    Image image = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("jweter2.png"))
    application.setDockIconImage(image);
}
catch (Exception e) {
    // Ignore. Will happen when not running on OSX. And even on OSX, an exception here is harmless.
}


// Add method getSizeAsReadableString() to java.io.File
java.io.File.metaClass.getSizeAsReadableString = { 
    long size = delegate.length()
    int exponent = 0
    def relativeSize = size     // Intentionally using def: runtime type will change to float after division    
    while (relativeSize >= 1024) {
        relativeSize /= 1024
        exponent += 1
    }
    def unit
    switch(exponent) {
        case 0: unit = "bytes"; break;
        case 1: unit = "KB"; break;
        case 2: unit = "MB"; break;
        case 3: unit = "GB"; break;
        case 4: unit = "TB"; break;
        default: throw new Exception("file size to large to handle: ${size}")
    }
    def value = relativeSize as String
    value = value.length() > 4? value[0..3]: value   // Max 3 digits (and one decimal separator)
    if (value.endsWith("."))
        value = value[0..-2]
    value + ' ' + unit
}


// Parse command line options.
def cli = new CliBuilder(usage: 'JWeter [-c|-v] [file]')
cli.with {
    c longOpt: 'clean',   'Remove all stored preferences'
    h longOpt: 'help',    'Show usage'
    v longOpt: 'version', 'Show version (and exit)'
}

def options = cli.parse(args)
if (options.arguments()?.getAt(0)?.startsWith('-')) {
    println "Invalid option '${options.arguments()[0]}'"
    cli.usage()
    return
}

def wizzard = new Wizzard()

if (options.h) {
    println "JWeter version ${wizzard.version}"
    cli.usage()
    return
}
if (options.v) {
    def version = wizzard.version
    if (version) {
        println "JWeter version ${version}"
    }
    else {
        println "JWeter development release; build number ${wizzard.buildNr?:"unknown"}"
    }
    println "JWeter is free software: you can redistribute it and/or modify\n" +
            "it under the terms of the GNU General Public License."
    println "Source: git@bitbucket.org:pjtr/jweter.git"
    println "JWeter is created by Peter Doornbosch."
    return
}

if (options.c) {
    wizzard.clearLru()
}


// Start the GUI: show the JWeter wizzard
wizzard.execute(options.arguments()? options.arguments()[0]: null)

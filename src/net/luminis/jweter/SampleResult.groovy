/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

/**
 * Simple value object for holding all relevant data for one JTL sample.
 */
class SampleResult {
    String label
    long timestamp
    int time
    boolean success
    String responseCode
    String failure

    /**
     * Return error cause.
     * The nasty thing is that JMeter can records multiple errors in one sample. E.g. a 404 or 500 is an error in
     * itself, but if the sample has an assertion too, that one is very likely to fail also.
     * Our approach is that http errors or more important than assertion failures.
     */
    String getErrorCause() {
        // Only call if there is an error...
        assert !success
        if (isErrorCode(responseCode)) {
            return responseCode
        }
        else if (! responseCode.isNumber()) {
            if (responseCode.startsWith('Non HTTP response code: ')) {
                // Strip off that part
                return responseCode['Non HTTP response code: '.size() .. -1]
            }
            else {
                return responseCode
            }
        }
        else if (failure) {
            return failure
        }
        else {
            // Unknown error / failure
            // Poor man's logging: print to stdout.
            println "Unknown failure reason for sample ${this}"
            return 'unknown'
        }
    }

    void setAssertionFailure(boolean value) {
        if (value) {
            failure = "assertion failure"
        }
        else {
            failure = null
        }
    }

    void setAssertionError(boolean value) {
        if (value) {
            failure = "assertion error"
        }
        else {
            failure = null
        }
    }

    String toString() {
        "[${label}: ${timestamp}->${time}/${success}]"
    }

    boolean isErrorCode(String httpStatusCode) {
        return httpStatusCode.startsWith('4') || httpStatusCode.startsWith('5')
    }
}


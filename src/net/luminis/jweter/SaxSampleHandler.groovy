/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.Attributes

/**
 * Sax handler for parsing JMeter (.jtl) samples.
 */
class SaxSampleHandler extends DefaultHandler {

    def list = []
    def current
    def currentTextNodeValue
    def count = 0
    int level = 0
    boolean jtl12
    boolean inAssertionElement

    void startElement(String ns, String localName, String qName, Attributes attrs) {

        switch (qName) {
            case 'testResults':
                jtl12 = attrs.getValue('version') == '1.2'
                break
            case 'httpSample':
            case 'sample':
                if (level == 0) {
                    current = new SampleResult()
                    current.label = attrs.getValue('lb')
                    current.timestamp = attrs.getValue('ts') as long
                    current.time = attrs.getValue('t') as long
                    current.success = new Boolean(attrs.getValue('s'))
                    current.responseCode = attrs.getValue('rc')
                    list << current
                }
                level++
                break
            case 'assertionResult':
                inAssertionElement = true
                break
        }
    }

    void characters(char[] chars, int offset, int length) {
        currentTextNodeValue = new String(chars[offset..(offset+length-1)] as char[])
    }

    void endElement(String ns, String localName, String qName) {

        switch (qName) {
           case 'httpSample':
           case 'sample':
               level--
               if (level == 0) {
                   count++
               }
               current = null
               break
            case 'assertionResult':
                inAssertionElement = false
                break
            case 'failure':
                if (inAssertionElement && new Boolean(currentTextNodeValue)) {
                    current.assertionFailure = true
                }
                break
            case 'error':
                if (inAssertionElement && new Boolean(currentTextNodeValue)) {
                    current.assertionError = true
                }
                break
        }
    }
}

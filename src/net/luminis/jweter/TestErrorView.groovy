package net.luminis.jweter

if (!args) {
    println "Expected argument: jtl file"
    System.exit(1)
}

def processor = new JMeterResults()
processor.file = new File(args[0])
processor.parseWithSax(processor.file, null)
processor.selectedLabels = processor.labels
processor.createErrorAnalysis(true)
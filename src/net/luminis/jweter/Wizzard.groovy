/**
 * Copyright 2012 Peter Doornbosch
 *
 * This file is part of JWeter, an analysis and visualization tool for JMeter .jtl files.
 *
 * JWeter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JWeter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.jweter

import groovy.swing.SwingBuilder
import javax.swing.WindowConstants as WC
import java.awt.FlowLayout
import java.awt.BorderLayout
import javax.swing.JFileChooser
import javax.swing.JOptionPane
import java.util.prefs.Preferences
import javax.swing.DefaultComboBoxModel
import java.awt.Component
import java.awt.Color
import java.awt.Point
import javax.swing.JLabel

import javax.swing.border.EmptyBorder
import javax.swing.BoxLayout
import javax.swing.ImageIcon
import java.awt.SplashScreen
import javax.swing.SwingUtilities

/**
 * The JWeter UI: a wizzard.
 */
class Wizzard {

    String getVersion() {
        def versionTag = this.getClass().getResource("version.properties")?.readLines()?.getAt(0)
        def matcher = versionTag =~ /v([\d.]+)/
        matcher.matches() ? matcher[0][1]: ""
    }

    String getBuildNr() {
        def versionTag = this.getClass().getResource("version.properties")?.readLines()?.getAt(0)
        def matcher = versionTag =~ /v([\d.]+)(-(\d+)-\w+)/
        matcher.matches() ? "${matcher[0][1]}${matcher[0][2]}": ""
    }

    def swing = new SwingBuilder()
    def frame = swing.frame(title: 'JWeter', locationByPlatform: true, defaultCloseOperation: WC.EXIT_ON_CLOSE)
    
    def error(String message) {
        def icon = new ImageIcon(this.getClass().getResource("jweter3_54.png"))
        JOptionPane.showMessageDialog frame, message, "Error", JOptionPane.ERROR_MESSAGE, icon
    }

    def execute(String fileArg) {
        def lru = new DefaultComboBoxModel(getLru().toArray())

        swing.actions
        {
            action(id: 'prev',
                    name: '<<',
                    closure: { layout.previous(frame.contentPane) })
            action(id: 'next',
                    name: '>>',
                    closure: { layout.next(frame.contentPane) })
        }

        swing.container(frame) {

            def resultsProcessor
            def selectList = []

            layout = cardLayout(new BetterCardLayout())

            // Step 1: select JMeter results file
            panel(new HooksPanel(), constraints: '1') {
                borderLayout()
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Select JMeter results (.jtl) file '), parent: true)
                    tableLayout(cellpadding: 3) {
                        tr {
                            td { label 'File: ' }
                            td {
                                comboBox(id: 'inputFile', editable: true, model: lru, preferredSize: [300, 28],
                                        actionPerformed: { event ->
                                            if (event.actionCommand == "comboBoxChanged") {
                                                def file = new File(swing.inputFile.selectedItem)
                                                if (file.exists()) {
                                                    swing.fileSize.text = file.sizeAsReadableString
                                                }
                                                else {
                                                    swing.fileSize.text = ''
                                                    error "File '${swing.inputFile.selectedItem}' does not exist."
                                                }
                                            }
                                        })
                            }
                            td {
                                button(text: '...', actionPerformed: {
                                    def dir = new File(".")
                                    if (swing.inputFile.selectedItem) {
                                        dir = new File(swing.inputFile.selectedItem).parent
                                    }
                                    fc = new JFileChooser(dir)
                                    if (fc.showOpenDialog() == JFileChooser.APPROVE_OPTION) {
                                        swing.fileSize.text = fc.selectedFile.sizeAsReadableString

                                        def selectedFile = fc.selectedFile.absolutePath
                                        swing.inputFile.addItem(selectedFile)
                                        swing.inputFile.setSelectedItem(selectedFile)
                                    }
                                })
                            }
                        }
                        tr {
                            td(colspan: 2) {
                                panel {
                                    flowLayout(hgap: 0)
                                    label(text: 'File size: ')
                                    label(id: 'fileSize', text: swing.inputFile.selectedItem? new File(swing.inputFile.selectedItem).sizeAsReadableString: '')   
                                }
                            }
                            td { }
                        }
                        tr {
                            def icon = new ImageIcon(this.getClass().getResource("jweter1.png"))
                            td(colspan: 3) { label(id: 'icon', icon: icon) }
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    boxLayout()
                    panel {
                        flowLayout(alignment: FlowLayout.LEFT)
                        button(text: "?", actionPerformed: {
                            def about = dialog(title: 'About JWeter', resizable: false, id: 'aboutDlg') {
                                borderLayout()
                                panel(constraints: BorderLayout.CENTER, border: compoundBorder([emptyBorder(5), raisedBevelBorder(), loweredBevelBorder(), etchedBorder(), emptyBorder(10)])) {
                                    boxLayout(axis: BoxLayout.Y_AXIS)
                                    label "JWeter version ${version}${buildNr}"
                                    label 'uses the open source JFreeChart chart library'
                                    label 'author: Peter Doornbosch'
                                    label 'contributions: Daan van Berkel'
                                    label 'graphics: Ivo Domburg'
                                    label 'development environment provided by Luminis'
                                }
                                panel(constraints: BorderLayout.SOUTH) {
                                    button(text: 'Ok', actionPerformed: { swing.aboutDlg.visible = false })
                                }
                            }
                            about.locationRelativeTo = frame
                            about.pack()
                            about.show()
                        }).putClientProperty("JButton.buttonType", "help")  // Looks nice on OSX, but on others?  ;-)
                    }
                    panel {
                        flowLayout(alignment: FlowLayout.RIGHT)
                        button(text: '>>', actionPerformed: {
                            def inputFile = swing.inputFile.selectedItem
                            if (!inputFile || !new File(inputFile).isFile()) {
                                error 'Select a JMeter (.jtl) result file first.'
                            }
                            else {
                                addLru(inputFile)
                                selectList.clear()
                                def worker = new JtlFileReaderWorker(inputFile, layout, frame)
                                resultsProcessor = worker.resultsProcessor
                                worker.execute()
                            }
                        })
                    }
                }
            }

            // Step 2: show file info
            panel(new HooksPanel(id: "dizze"), constraints: '2',
                    enterHook: {
                        // Fill dialog with file details
                        builder.fileName.text = resultsProcessor.file.name as String
                        builder.fileDir.text = resultsProcessor.file.parent as String
                        builder.fileInfoFileSize.text = resultsProcessor.file.sizeAsReadableString
                        builder.nrOfSamples.text = String.format("%,d", resultsProcessor.nrOfSamples)
                        builder.startTest.text = resultsProcessor.startTest.dateTimeString
                        builder.endTest.text = resultsProcessor.endTest.dateTimeString
                        builder.duration.text = resultsProcessor.duration as String
                        builder.fastest.text = resultsProcessor.fastest as String
                        builder.slowest.text = resultsProcessor.slowest as String
                    }) {
                borderLayout()
                current.builder = swing
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' JMeter test results '), parent: true)
                    gridLayout(cols: 1, rows: 1)    // This gridlayout will 'protect' the border (box takes more space than available)
                    panel(border:new EmptyBorder(0,10,0,10)) {
                        boxLayout()
                        panel {
                            gridLayout(cols: 1, rows: 0)
                            label('File:', horizontalAlignment: JLabel.RIGHT)
                            label('Directory:', horizontalAlignment: JLabel.RIGHT)
                            label('File size:', horizontalAlignment: JLabel.RIGHT)
                            label('Start of test:', horizontalAlignment: JLabel.RIGHT)
                            label('End of test:', horizontalAlignment: JLabel.RIGHT)
                            label('Number of samples:', horizontalAlignment: JLabel.RIGHT)
                            label('Test duration (seconds):', horizontalAlignment: JLabel.RIGHT)
                            label('Fastest response:', horizontalAlignment: JLabel.RIGHT)
                            label('Slowest response:', horizontalAlignment: JLabel.RIGHT)
                        }
                        hstrut(width: 10)
                        panel() {
                            gridLayout(cols: 1, rows: 0)
                            label(id: 'fileName')
                            label(id: 'fileDir')
                            label(id: 'fileInfoFileSize')
                            label(id: 'startTest')
                            label(id: 'endTest')
                            label(id: 'nrOfSamples')
                            label(id: 'duration')
                            label(id: 'fastest')
                            label(id: 'slowest')
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout(alignment: FlowLayout.RIGHT)
                    button(action: prev)
                    button(action: next)
                }
            }

            // Step 3: select tags
            def defaultSelectValue = true
            panel(new HooksPanel(), constraints: '3',
                    enterHook: {
                        if (!selectList) {
                            resultsProcessor.labels.each { label ->
                                def minMaxValues = resultsProcessor.getCountMinMax(label)
                                selectList.add([defaultSelectValue, label] + minMaxValues)
                            }
                            builder.labelTable.invalidate();
                        }
                    },
                    leaveHook: {
                        selectList.each { resultsProcessor.selectLabel(it[1], it[0]) }
                        def any = selectList.any { it[0] }
                        if (!any) {
                            error("Select at least one label...")
                        }
                        return any
                    })
                    {
                        current.builder = swing
                        borderLayout()
                        panel {
                            def bgColor = current.background
                            compoundBorder(outer: emptyBorder(13),
                                    inner: compoundBorder(outer: titledBorder(' Select the results to analyse '), inner: emptyBorder(13)), parent: true)
                            borderLayout()
                            scrollPane(id: 'scroller', constraints: BorderLayout.CENTER) {
                                current.viewport.background = Color.LIGHT_GRAY
                                table(id: 'labelTable', background: bgColor, preferredScrollableViewportSize: [454, 202]) {
                                    tableModel(list: selectList) {
                                        closureColumn(header: 'select', type: Boolean.class, preferredWidth: 50, read: { it[0] }, write: { list, value -> list[0] = value })
                                        closureColumn(header: 'name', preferredWidth: 300, read: { it[1] })
                                        closureColumn(header: '# samples', preferredWidth: 50, read: { it[2] })
                                        closureColumn(header: 'min', preferredWidth: 50, read: { it[3] })
                                        closureColumn(header: 'max', preferredWidth: 100, read: { it[4] })
                                    }
                                }
                            }
                            panel(constraints: BorderLayout.SOUTH) {
                                def tableModel = swing.labelTable.model
                                flowLayout(alignment: FlowLayout.LEFT, vgap: 3)
                                button(text: 'all', actionPerformed: {
                                    selectList.each { row -> row[0] = true };
                                    tableModel.fireTableDataChanged()
                                } )
                                tableModel.fireTableDataChanged()
                                button(text: 'none', actionPerformed: {
                                    selectList.each { row -> row[0] = false };
                                    tableModel.fireTableDataChanged()
                                } )
                            }
                        }
                        panel(constraints: BorderLayout.SOUTH) {
                            flowLayout().alignment = FlowLayout.RIGHT
                            button(action: prev)
                            button(action: next)
                        }
                    }

            // Step 4: select graph type.
            panel(new HooksPanel(), constraints: '4',
                    enterHook: { },
                    leaveHook: { })
                    {
                        current.builder = swing
                        borderLayout()
                        panel(constraints: BorderLayout.CENTER) {
                            compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Select graph type '), parent: true)
                            vbox {
                                buttonGroup().with { radioGroup ->
                                    hbox(alignmentX: Component.LEFT_ALIGNMENT,) {
                                        def rb1 = radioButton(id: 'select_scatter', buttonGroup: radioGroup)
                                        label(text: "Time series (scatter diagram)", mouseClicked: { rb1.selected = true })
                                    }
                                    hbox(alignmentX: Component.LEFT_ALIGNMENT) {
                                        def rb2 = radioButton(id: 'select_distr', buttonGroup: radioGroup)
                                        label(text: "Distribution Histogram", mouseClicked: { rb2.selected = true })
                                    }
                                    hbox(alignmentX: Component.LEFT_ALIGNMENT) {
                                        def rb3 = radioButton(id: 'select_analysis', buttonGroup: radioGroup, selected: true)
                                        label(text: "Error analysis", mouseClicked: { rb3.selected = true })
                                    }
                                }
                            }
                        }
                        panel(constraints: BorderLayout.SOUTH) {
                            flowLayout().alignment = FlowLayout.RIGHT
                            button(action: prev)
                            button(text: '>>', actionPerformed: {
                                if (swing.select_scatter.selected) {
                                    layout.show(frame.contentPane, '6')
                                }
                                else if (swing.select_distr.selected) {
                                    layout.next(frame.contentPane)
                                }
                                else {
                                    resultsProcessor.createErrorAnalysis(false)
                                }
                            })
                        }
                    }

            // Step 5: Histogram
            panel(new HooksPanel(), constraints: '5', enterHook: {
                builder.mmax.text = resultsProcessor.getMaxForSelectedLabels()
            }) {
                current.builder = swing
                borderLayout()
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Histogram options '), parent: true)
                    tableLayout(cellpadding: 3) {
                        tr {
                            td { label 'Min: ' }
                            td { min = textField(columns: 3, text: '0') }
                        }
                        tr {
                            td { label 'Max: ' }
                            td { max = textField(columns: 6, id: 'mmax') }
                        }
                        tr {
                            td { label 'Bins: ' }
                            td { bin = textField(text: '10', columns: 6, id: 'mbin') }
                        }
                        tr {
                            td { label 'Relative frequency: ' }
                            td { relative = checkBox() }
                        }
                        tr {
                            td { label 'All together: ' }
                            td { together = checkBox() }
                        }
                        tr {
                            td { }
                            td {
                                button(text: "graph", actionPerformed: {
                                    resultsProcessor.createHistogram(bin.text as int, min.text as int, max.text as int, relative.selected, together.selected) })
                            }
                        }
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(action: prev)
                    button(text: '>>', actionPerformed: {
                        try {
                            if ((bin.text as int) < 1) {
                                error "Invalid input: number of bins must be at least 1"
                            }
                            else if ((min.text as int) >= (max.text as int)) {
                                error "Invalid input: min must be less than max."
                            }
                            else {
                                resultsProcessor.createHistogram(bin.text as int, min.text as int, max.text as int, relative.selected, together.selected)
                            }
                        }
                        catch (NumberFormatException e) {
                            error "Invalid input (not a number)"
                        }
                    })
                }
            }
            // Step 6: Scatterplot
            panel(constraints: "6") {
                borderLayout()
                def logAxisBtn
                def relativeTimeBtn
                panel(constraints: BorderLayout.CENTER) {
                    compoundBorder(outer: emptyBorder(13), inner: titledBorder(' Time series options '), parent: true)
                    vbox {
                        logAxisBtn = checkBox(text: 'use logarithmic axis')
                        relativeTimeBtn = checkBox(text: 'use relative time for X-axis')
                        button(text: "graph", actionPerformed: { resultsProcessor.createGraph(logAxisBtn.selected, relativeTimeBtn.selected) })
                    }
                }
                panel(constraints: BorderLayout.SOUTH) {
                    flowLayout().alignment = FlowLayout.RIGHT
                    button(text: '<<', actionPerformed: { layout.show(frame.contentPane, '4') })
                    button(text: '>>', actionPerformed: { resultsProcessor.createGraph(logAxisBtn.selected, relativeTimeBtn.selected) })
                }
            }
        }

        frame.pack()
        def splashBounds = SplashScreen.getSplashScreen()?.getBounds()
        if (splashBounds) {
            Point iconCoord = SwingUtilities.convertPoint(swing.icon, 0, 0, null)
            frame.location = new Point(splashBounds.@x - iconCoord.@x, splashBounds.@y - iconCoord.@y)
        }
        frame.show()

        if (fileArg) {
            swing.inputFile.selectedItem = new File(fileArg).absolutePath
        }
    }

    List getLru() {
        def lru = Preferences.userNodeForPackage(this.class).node("lru")
        def count = lru.keys().size()
        count ? (0..count - 1).collect { lru.get(it as String, null) } : []
    }

    void addLru(String newItem) {

        def lru = Preferences.userNodeForPackage(this.class).node("lru")
        def count = lru.keys().size()
        def currentLruItems = count ? (0..count - 1).collect { lru.get(it as String, null) } : []

        def newLruItems = [newItem] + (currentLruItems - newItem)

        int max = 5
        newLruItems.eachWithIndex { value, index ->
            if (index < max) {
                lru.put(index as String, value)
            }
        }
    }

    void clearLru() {
        Preferences.userNodeForPackage(this.class).removeNode()
    }
}